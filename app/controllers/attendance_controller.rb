require 'date'

class AttendanceController < ApplicationController
  unloadable
  before_filter :only => :index

  def index
    # Check whether parameter from client by post method exist or not.
    # And set up user instance value here.
    @target_user = nil

    begin
      user_id = params[:select_user]
      @target_user = User.find(user_id.to_i)
      if params[:save_flag] != nil and params[:save_flag].to_i == 1
        # cv = CustomValue.find(:first, :conditions => { :customized_id => @target_user.id, :custom_field_id => 171})
        cv = CustomValue.where(customized_id: @target_user.id, custom_field_id: 171).first
        unless cv == nil
          cv.value = params[:init_comp].to_f
          cv.save
        else
          CustomValue.create(:customized_id => @target_user.id, :customized_type => 'Principal', :custom_field_id => 171, :value => params[:init_comp].to_f )
        end

        #cv = CustomValue.find(:first, :conditions => { :customized_id => @target_user.id, :custom_field_id => 172})
        cv = CustomValue.where(customized_id: @target_user.id, custom_field_id: 172).first
        unless cv == nil
          cv.value = params[:init_pto].to_f
          cv.save
        else
          CustomValue.create(:customized_id => @target_user.id, :customized_type => 'Principal', :custom_field_id => 172, :value => params[:init_pto].to_f )
        end

        #cv = CustomValue.find(:first, :conditions => { :customized_id => @target_user.id, :custom_field_id => 180})
        cv = CustomValue.where(customized_id: @target_user.id, custom_field_id: 180).first
        date = Date.strptime(params[:date], "%Y-%m-%d") rescue nil
        unless cv == nil
          cv.value = date unless date == nil
          cv.save
        else
          CustomValue.create(:customized_id => @target_user.id, :customized_type => 'Principal', :custom_field_id => 180, :value => date ) unless date == nil
        end
      end
    rescue => e
      p e
    ensure
      @target_user = User.current if @target_user == nil
    end

    # Create Absent Object here!!
    @absent = Absent.new @target_user

    # Grab All Issues that are related with user's holidays
    #Issue.find(:all, :conditions => ["project_id = ? and author_id = ? and start_date >= ?", 836, @target_user.id, @absent.enable_start_date]).each do |issue|
    Issue.where("project_id = ? and author_id = ? and start_date >= ?", 836, @target_user.id, @absent.enable_start_date).each do |issue|
      # absent_for = CustomValue.find(:first, :conditions => { :customized_id => issue.id, :custom_field_id => 83}).value # absent for
      absent_for = CustomValue.where(customized_id: issue.id, custom_field_id: 83).first.value
      # type = CustomValue.find(:first, :conditions => { :customized_id => issue.id, :custom_field_id => 92}).value # type
      type = CustomValue.where(customized_id: issue.id, custom_field_id: 92).first.value

      @absent.add_track(issue.start_date, issue.due_date, type, absent_for)
    end
  end
end



