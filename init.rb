require 'user_listener'
require 'user_custom_value_patch'

Rails.configuration.to_prepare do
  require_dependency 'application_helper'
  CustomFieldsHelper.send(:include, UserCustomValuePatch)
end

Redmine::Plugin.register :attendance do
  name 'Attendance plugin'
  author 'Hidekazu Inoue'
  description 'Attendance Meue and Monkey Patch for authorities of leaves update'
  version '0.1.2'
  url 'http://'
  author_url 'http://'

  permission :attendance, {:attendance => [:index, :change]}

  menu :account_menu, :attendance, { :controller => 'attendance', :action => 'index' }, :caption => "Attendance", :after => :my_account, :if => Proc.new {
      User.current.allowed_to?(:attendance, nil, :global => true)
    }
 
#  settings :default => {'empty' => true}, :partial => 'settings/attendance'
end

# rails generate redmine_plugin_controller Attendance attendance index



