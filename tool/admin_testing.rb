require "net/http"
require "uri"
require 'rexml/document'
require "date"
require 'rubygems'
require 'active_support/all'



HOST = "54.64.58.116"
PORT = 80
API_KEY = "1b86e7707bc649478a176be1d1c439d0a2273f0a"


GROUP_ID_TK = 231
GROUP_ID_SH = 232
GROUP_ID_LA = 233
GROUP_ID_HK = 230

LIMIT = 100

LOCATIONS_LIST = ["tk", "sh", "la", "hk"]



MY_PERFORMANCE_REVIEW_PROJECT_ID = 833
MY_PERFORMANCE_REVIEW_SUBJECT = "Annual Staff Review Required"
TRACKER_ID_ANNUAL_REVEIW = 37
MY_ACCOUNT_REVIEW_SUBJECT = "Annual PTO Update required"

class BillingMaker
  def initialize()
    @today = Date.today
    @month_label = @today.strftime("%Y/%m")
    @project_map = {}
    @performance_review_ticket_map = {}
    @users = {}
    @no_start_date_list = []
    @group_list = {}
  end



  def http_get(uri, send_data)

    response = nil

    https = Net::HTTP.new(uri.host, uri.port)



    query_string = (send_data||{}).map{|k,v|
      URI.encode(k.to_s) + "=" + URI.encode(v.to_s)
    }.join("&")

    request = Net::HTTP::Get.new(uri.path + "?" + query_string)
    response = https.request(request)

    response.body rescue nil
  end

  def grab_performance_reviews
    uri = URI.parse("http://54.64.58.116/issues.xml")
    continue = true
    offset = 0

    while continue
      send_data = {
        :key => API_KEY,
        :project_id => MY_PERFORMANCE_REVIEW_PROJECT_ID.to_s,
        :limit => LIMIT.to_s,
        :offset => offset.to_s,
        :tracker_id => TRACKER_ID_ANNUAL_REVEIW.to_s #Annual Reveiw
      }

      response_data = http_get(uri, send_data)
      doc = REXML::Document.new(response_data)

      total_count =  doc.elements['issues'].attributes["total_count"].to_i
      limit = doc.elements['issues'].attributes["limit"].to_i
      offset = doc.elements['issues'].attributes["offset"].to_i

      doc.elements.each('issues/issue') do |element|
        assigned_to_id = element.elements["assigned_to"].attributes["id"].to_i
        start_date = Date.strptime(element.elements["start_date"].text, "%Y-%m-%d")

        if start_date > Date.today - 60
          @performance_review_ticket_map[assigned_to_id] = {
            :subject => element.elements["subject"].text,
            :start_date => start_date,
            :assigned_to_name => element.elements["assigned_to"].attributes["name"],
            :assigned_to_id => assigned_to_id
          }
        end
      end

      offset += limit
      continue = false if total_count < limit + offset
    end
  end

  def grab_groups
    LOCATIONS_LIST.each do |location|
      case location
      when "tk"
        location_id = GROUP_ID_TK
      when "sh"
        location_id = GROUP_ID_SH
      when "la"
        location_id = GROUP_ID_LA
      when "hk"
        location_id = GROUP_ID_HK
      end

      uri = URI.parse("https://#{HOST}:#{PORT}/groups/#{location_id}.xml")
      continue = true
      offset = 0

      send_data = {
        :key => API_KEY,
        :include => "users"
      }

      @group_list[location_id] = {}

      response_data = http_get(uri, send_data)
      doc = REXML::Document.new(response_data)

      #p doc.elements['group/id'].text.to_i
      @group_list[location_id][:name] = doc.elements['group/name'].text

      doc.elements.each('group/users/user') do |element|
        p element.attributes["id"].to_i
        p element.attributes["name"]
        mail = @users[element.attributes["id"].to_i][:mail] rescue nil
        p mail
        @group_list[location_id][element.attributes["id"].to_i] = {:id => element.attributes["id"].to_i, :name => element.attributes["name"], :mail => mail}
      end
    end
  end

  def grab_users

    uri = URI.parse("https://#{HOST}:#{PORT}/users.xml")
    continue = true
    offset = 0

    while continue
      send_data = {
        :key => API_KEY,
        :limit => LIMIT.to_s,
        :offset => offset.to_s
      }

      response_data = http_get(uri, send_data)
      doc = REXML::Document.new(response_data)

      total_count =  doc.elements['users'].attributes["total_count"].to_i
      limit = doc.elements['users'].attributes["limit"].to_i
      offset = doc.elements['users'].attributes["offset"].to_i

      doc.elements.each('users/user') do |element|
        hash = {}
        hash[:id] = element.elements["id"].text.to_i
        hash[:login] = element.elements["login"].text
        hash[:mail] = element.elements["mail"].text
        hash[:name] = element.elements["firstname"].text + " " + element.elements["lastname"].text

        element.elements["custom_fields"].each do |subelement|
          custom_field_id = subelement.attributes["id"]
          if custom_field_id == "97" # location
          case subelement.elements["value"].text.to_s.downcase
          when "tk"
            hash[:location] = GROUP_ID_TK
          when "sh"
            hash[:location] = GROUP_ID_SH
          when "la"
            hash[:location] = GROUP_ID_LA
          when "hk"
            hash[:location] = GROUP_ID_HK
          end
          end

          hash[:division] = subelement.elements["value"].text if custom_field_id == "101" # division
          hash[:manager] = subelement.elements["value"].text if custom_field_id == "153" # manager
          hash[:start_date] = Date.strptime(subelement.elements["value"].text, "%Y-%m-%d") rescue nil if custom_field_id == "110" # start date

        end

        unless hash[:start_date] == nil
          if hash[:mail] =~ /fusionsystems/ or hash[:mail] =~ /ingeniumgroup/
            @users[element.elements["id"].text.to_i] = hash
          end
        else
          @no_start_date_list << hash[:login]
        end
      end

      offset += limit
      continue = false if total_count < limit + offset
    end
  end

  #TODO Fix
  def find_user_by_name(name)
    @users.each do |k, value|
      return value if value[:login].downcase == name.gsub(" ",".").downcase
    end
    return nil
  end

  def post_to_redmine(data)
    uri = URI.parse("http://#{HOST}:#{PORT}/issues.json")
    response = nil

    Net::HTTP.start(uri.host, uri.port){|http|

      request = Net::HTTP::Post.new(uri.path)




      request.content_type = 'application/json'
      request['X-Redmine-API-Key'] = API_KEY
      request.body = data

      response = http.request(request)

      p response
    }
  end

  def create_json(user, year)
    data = <<-EOF
{
    "issue": {
      "project_id": "#{MY_PERFORMANCE_REVIEW_PROJECT_ID.to_s}",
      "subject": "#{year} #{MY_PERFORMANCE_REVIEW_SUBJECT}",
      "tracker_id": "#{TRACKER_ID_ANNUAL_REVEIW.to_s}",
      "description": "Start Date is #{user[:start_date]}. This issue was created by Redmine Client tool automatically. ",
      "assigned_to_id": "#{user[:id]}"
    }
}
EOF
  end

  def executer
    self.grab_performance_reviews
    self.grab_users
    self.grab_groups

    today = Date.today

    @users.each do |user_id, user|
      puts "#{user[:login]} #{user[:start_date]}"

      if today.month != 12
        target = user[:start_date].change(:year => today.year)
        days = (target - today).days
      else
        target = user[:start_date].change(:year => (today.years_since 1).year)
        days = (target - today).days
      end

      if today < target and target < (today.months_since 1)
        p target
        p today
        p (today.months_since 1)
        p days
        unless @performance_review_ticket_map.key? user_id
          puts "Creat ticket #{@performance_review_ticket_map[user_id].to_s}"
          json = create_json(user, target.year.to_s)
          post_to_redmine json

          # set up mail
          puts "location: " + user[:location].to_s

          address_to = []
          address_cc = []

          @group_list[user[:location]].each do |k, v|
            address_cc << v[:mail] unless v[:mail] == nil
          end
          manager = find_user_by_name(user[:manager]) rescue nil
          p manager
          unless manager == nil
            address_to << manager[:mail]
          else
            puts "manager is null #{user[:name]}"
          end

          p address_to
          p address_cc

          address_cc << "hidekazu.inoue@fusionsytems.co.jp"

          subject = "#{MY_PERFORMANCE_REVIEW_SUBJECT} [#{user[:login]}]"

          p subject

          body = ""

#          SendMail.delivery(address_to, address_cc, subject, body)
           exit
        else
          puts "Already booked #{@performance_review_ticket_map[user_id].to_s}"
        end
      end
    end
  end

  def pto_setting_executer
    self.grab_performance_reviews
    self.grab_users
    self.grab_groups

    today = Date.today

    puts "PTO Setting Reminder"

    @users.each do |user_id, user|
      puts "#{user[:login]} Start Date#{user[:start_date].to_s} ---------------------------"

      if user[:comp_awarded] == nil
        days = today.mjd - user[:start_date].mjd

        # Over 6 months
        if days > 30 * 6
          address_to = []
          address_cc = []


          @group_list[user[:location]].each do |k, v|
            if v.class == "Hash"
              address_cc << v[:mail] unless v[:mail] == nil
            end
          end
          manager = find_user_by_name(user[:manager]) rescue nil

          unless manager == nil
            address_to << manager[:mail]
          else
            puts "manager is null #{user[:name]}"
          end

          address_cc << "jason.zudell@fusionsytems.co.jp"

          subject = "#{MY_ACCOUNT_REVIEW_SUBJECT} [#{user[:login]}]"

          p subject

          body = ""


          #SendMail.delivery(address_to, address_cc, subject, body)
        end
      end

    end

  end




end


module SendMail
  def self.delivery(receiver, receiver_cc, subject="test", body="body", attachments=[])
    options = {
    :address              => "tokyo.fusionsystems.org",
    :port                 => 25,
    :domain               => "tokyo.fusionsystems.org",
    :user_name            => "helpdesk@fusionsystems.org",
    :password             => "Password@",
    :authentication       => :login
    }

    Mail.defaults do
      delivery_method :smtp, options
    end

    mail = Mail.new do
      to receiver.join(", ")
      cc receiver_cc.join(", ")
      from "helpdesk@fusionsystems.org"
    end
    mail.charset ='utf-8'
    mail.subject = subject
    mail.body  = body
    attachments.each do |attachment|
      mail.add_file(attachment)
    end

    mail.deliver
  end

end


if __FILE__ == $0
  invoice_maker = BillingMaker.new()

  invoice_maker.pto_setting_executer
end
