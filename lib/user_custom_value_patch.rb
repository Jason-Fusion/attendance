module UserCustomValuePatch

  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)

    base.class_eval do
      alias_method_chain :custom_field_tag_with_label, :filter_tag
    end
  end

  module InstanceMethods
    EDITABLE_GROUP_NAME = "Fusion Systems Group - People Managers"
    HIDDEN_GROUP_NAME = "Hide_Members_Panel".upcase
    
    # Adds a rates tab to the user administration page
    def custom_field_tag_with_label_with_filter_tag(name, custom_value, options={})
      no_filter = true
      authority = false
      hidden_for_external_client = false
      value = ''
      
      custom_field = custom_value.custom_field
      if custom_field.id == 171 or custom_field.id == 172 or custom_field.id == 173 or custom_field.id == 175 or custom_field.id == 180
        
        User.current.groups.each { |group| return if group.lastname.upcase == HIDDEN_GROUP_NAME }
        
        no_filter = false
        
        User.current.groups.each { |group| authority = true if group.lastname == EDITABLE_GROUP_NAME }
        no_filter = true if authority
      
      end
      
      # Reviewer is also hidden for external clients
      if custom_field.id == 152 or custom_field.id == 153 or custom_field.id == 154
        User.current.groups.each { |group| return if group.lastname.upcase == HIDDEN_GROUP_NAME }
      end
      
      if no_filter
        custom_field_label_tag(name, custom_value, options) + custom_field_tag(name, custom_value)
      else
        value = ''
        value = custom_value.value unless custom_value.value == nil
        custom_field_label_tag(name, custom_value, options) + value
      end
    end
    
  end
end


