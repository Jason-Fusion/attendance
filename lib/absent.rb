require "date"

class Absent
  EDITABLE_GROUP_NAME = "Fusion Systems Group - People Managers"

  attr_accessor \
    :init_comp_days, :init_pto_days,                                                \
    :total_comp_days, :total_paid_days, :total_sick_days, :total_bere_days,         \
    :this_fy_comp_days, :this_fy_paid_days, :this_fy_sick_days, :this_fy_bere_days, \
    :pre_fy_comp_days, :pre_fy_paid_days, :pre_fy_sick_days, :pre_fy_bere_days,     \
    :tracks, :target_user, :this_fiscal_year, :pre_fiscal_year, :messages,          \
    :last_comp_day_awarded, :user_location, :start_date, :enable_start_date,        \
    :total_unpaid_days, :this_fy_unpaid_days, :pre_fy_unpaid_days, :target_user

  def initialize(target_user)
    today = DateTime.now
    if today.month < 4 or (today.month == 4 and today.day < 10) # Jan - Mar or before Apr.10
      @this_fiscal_year = DateTime.now.change(:month => 4, :day => 10).years_ago 1
    else
      @this_fiscal_year = DateTime.now.change(:month => 4, :day => 10)
    end
    @pre_fiscal_year = @this_fiscal_year.years_ago 1

    @target_user = target_user
    @authority = false

    @init_comp_days = 0.0
    @init_pto_days = 0.0

    @total_comp_days = 0.0
    @total_paid_days = 0.0
    @total_sick_days = 0.0
    @total_bere_days = 0.0
    @total_unpaid_days = 0.0

    @this_fy_comp_days = 0.0
    @this_fy_paid_days = 0.0
    @this_fy_sick_days = 0.0
    @this_fy_bere_days = 0.0
    @this_fy_unpaid_days = 0.0

    @pre_fy_comp_days = 0.0
    @pre_fy_paid_days = 0.0
    @pre_fy_sick_days = 0.0
    @pre_fy_bere_days = 0.0
    @pre_fy_unpaid_days = 0.0

    @last_comp_day_awarded = nil
    @user_location = nil
    @start_date = nil

    @tracks = []
    @messages = []
    @national_holidays = {}

    @enable_start_date = Date.strptime("2014-01-01", "%Y-%m-%d")

    check_authority
    grab_holidays
  end

  def add_track(start_date, end_date, type, absent_for)
    # Check args. If end_date is null, we alert here and use start_date as end_date as well.
    if end_date == nil
      @messages << "End Date(Due Date) was blank. So %s is set as End Date." % start_date.to_s
      end_date = start_date
    end

    if absent_for == 'Full Day(s)'
      unit = 1.0
    else
      unit = 0.5
    end

    date = start_date
    days = 0.0
    100.times do
      # Weekday? (Monday - Friday)
      if date.wday > 0 and date.wday < 6
        # Not National Holiday?
        unless @national_holidays.has_key?( date )
          days += unit
          if type == 'Comp Day'
            @total_comp_days += unit
            @this_fy_comp_days += unit if this_fisical_year?(date)
            @pre_fy_comp_days += unit  if pre_fisical_year?(date)
          elsif type == 'Paid Leave' or type == 'Paid Leave (PTO)'
            @total_paid_days += unit
            @this_fy_paid_days += unit if this_fisical_year?(date)
            @pre_fy_paid_days += unit  if pre_fisical_year?(date)
          elsif type == 'Sick Day' or type == 'Sick Leave (PTO)' or type == 'Sick Day (PTO)'
            @total_sick_days += unit
            @this_fy_sick_days += unit if this_fisical_year?(date)
            @pre_fy_sick_days += unit  if pre_fisical_year?(date)
          elsif type == 'Bereavement Leave'
            @total_bere_days += unit
            @this_fy_bere_days += unit if this_fisical_year?(date)
            @pre_fy_bere_days += unit  if pre_fisical_year?(date)
          elsif type == 'Unpaid Time Off'
            @total_unpaid_days += unit
            @this_fy_unpaid_days += unit if this_fisical_year?(date)
            @pre_fy_unpaid_days += unit  if pre_fisical_year?(date)
          end
        else
          @messages << "%s is %s. So this day was uncounted." % [date, @national_holidays[date]]
        end
      end

      break if date == end_date

      date += 1
    end

    param = {:start_date => start_date, :end_date => end_date, :type => type, :days => days}
    @tracks << AbsentTrack.new(param)
  end

  def total_pto_days
    return @total_paid_days + @total_sick_days
  end

  def this_fy_pto_days
    return @this_fy_paid_days + @this_fy_sick_days
  end

  def pre_fy_pto_days
    return @pre_fy_paid_days + @pre_fy_sick_days
  end

  def remain_fy_pto_days
    return @init_pto_days - @this_fy_paid_days - @this_fy_sick_days
  end

  def remain_fy_comp_days
    return @init_comp_days - @this_fy_comp_days
  end

  def can_see_others?
    return @authority
  end

  def this_fiscal_year_end
    @this_fiscal_year.years_since(1) - 1
  end

  def pre_fiscal_year_end
    @pre_fiscal_year.years_since(1) - 1
  end

  class AbsentTrack
    attr_accessor :start_date, :end_date, :type, :days
    def initialize(values)
      values.each do |k, v|
        self.send("#{k}=", v)
      end
    end
  end

  private
  # Check current user has authority which can see another person's absent status
  def check_authority
    User.current.groups.each do |group|
      @authority = true if group.lastname == EDITABLE_GROUP_NAME
    end
  end

  def grab_holidays
    user_location = nil
    # CustomValue.find(:all, :conditions => { :customized_id => [@target_user.id]}).each do |cv|
    CustomValue.where(customized_id: [@target_user.id]).each do |cv|
      @init_comp_days = cv.value.to_f if cv.custom_field_id == 171
      @init_pto_days = cv.value.to_f if cv.custom_field_id == 172
      @user_location = cv.value if  cv.custom_field_id == 97
      @last_comp_day_awarded = cv.value if  cv.custom_field_id == 180
      @start_date = cv.value if  cv.custom_field_id == 110
    end

    # Grab Holidays Information. We restrict by user's location.
    # Issue.find(:all, :conditions => ["project_id = ? and start_date >= ?", 830, @enable_start_date]).each do |issue|
    Issue.where("project_id = ? and start_date >= ?", 830, @enable_start_date).each do |issue|
      # cv = issue.custom_field_values.detect {|c| c.custom_field.name == "Event Type"}
      # event = cv.value
      # event = CustomValue.find(:first, :conditions => { :customized_id => issue.id, :custom_field_id => 141}).value # Event Type
      event = CustomValue.where(customized_id: issue.id, custom_field_id: 141).first.value
      if event == 'National Holiday'
        # cv = issue.custom_field_values.detect {|c| c.custom_field.name == "Event Type"}
        # location = cv.value
        # location = CustomValue.find(:first, :conditions => { :customized_id => issue.id, :custom_field_id => 91}).value # Location
        location = CustomValue.where(customized_id: issue.id, custom_field_id: 91).first.value
        if location == @user_location
          @national_holidays[issue.start_date] = issue.subject
        end
      end
    end
  end # end method

  def this_fisical_year?(date)
   return true if @this_fiscal_year <= date and this_fiscal_year_end >= date
   false
  end

  def pre_fisical_year?(date)
   return true if @pre_fiscal_year <= date and pre_fiscal_year_end >= date
   false
  end

end


